package com.test.sonar.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SonarController {
    
    public ResponseEntity<?> uncoveredController() {
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
