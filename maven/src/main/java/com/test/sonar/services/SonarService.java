package com.test.sonar.services;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SonarService {
    
    public void uncoveredMethod() {
        log.debug("Not covered method");
    }
}
